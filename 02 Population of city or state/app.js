const API_URL = 'https://gist.githubusercontent.com/Miserlou/c5cd8364bf9b2420bb29/raw/2bf258763cdddd704f8ffd3ea9a3e81d25e2c6f6/cities.json'
const dataContainer = document.querySelector('.suggestions')
const cityState = document.querySelector('.search')
const idhar = document.querySelector('.idhar')
let cities = [];

//Retrive the JSON
(async () => {
const resp = await fetch(API_URL)
const resJSON = await resp.json()
cities.push(...resJSON)
})()

//Event Listener for the typing
cityState.addEventListener('keydown', (e) => {
    console.log(e.target.value)
    checkData(e.target.value)
})

//Filter according to the input
function checkData(place){
    const filtered = cities.filter(CT => {
    const placeLowerCase = place.toLowerCase()
    const cityLowerCase = CT.city.toLowerCase()
    const stateLowerCase = CT.state.toLowerCase()
    return cityLowerCase.indexOf(placeLowerCase) !== -1 || stateLowerCase.indexOf(placeLowerCase) !== -1
})
createContainer(filtered)
}

//create DOM element for matching data
function createContainer(list){
    dataContainer.remove()
    idhar.innerHTML = ''
    const newDataEl = document.createElement('div')
    newDataEl.classList.add('filteredData')

    list.forEach(element => {
    const newDataStr =  `<ul>
                        <li>City : ${element.city}</li>
                        <li>State : ${element.state}</li>
                        <li>Population : ${element.population}</li>
                        </ul>`
    newDataEl.innerHTML += newDataStr
    idhar.appendChild(newDataEl)})
}