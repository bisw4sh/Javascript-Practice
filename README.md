01. Array Cardio
```URL = https://github.com/wesbos/JavaScript30/tree/master/04%20-%20Array%20Cardio%20Day%201```

02. Population of city/state
```URL = https://github.com/wesbos/JavaScript30/tree/master/06%20-%20Type%20Ahead```

03. Array Cardio 2
```URL = https://github.com/wesbos/JavaScript30/tree/master/07%20-%20Array%20Cardio%20Day%202```

04. Hold Shift and Check Checkboxes
```URL = https://github.com/wesbos/JavaScript30/tree/master/10%20-%20Hold%20Shift%20and%20Check%20Checkboxes```

05. Memory Game
```URL = https://youtu.be/bznJPt4t_4s```

06. Test Your Reaction Time
```URL = https://www.youtube.com/watch?v=1kDcKH7lK8A&list=WL&index=19&ab_channel=dcode```

07. Key Sequence Detection
```URL = https://github.com/wesbos/JavaScript30/tree/master/12%20-%20Key%20Sequence%20Detection```

08. Mouse Move Shadow
```URL = https://github.com/wesbos/JavaScript30/tree/master/16%20-%20Mouse%20Move%20Shadow```

09.  Sort Without Articles
```URL = https://github.com/wesbos/JavaScript30/tree/master/17%20-%20Sort%20Without%20Articles```

10. Adding Up Times with Reduce
```URL = https://github.com/wesbos/JavaScript30/tree/master/18%20-%20Adding%20Up%20Times%20with%20Reduce```

11. Data Structure in JS
