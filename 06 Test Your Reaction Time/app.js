const container = document.querySelector('.container')
const result = document.querySelector('.result')
const getRandTime = () => Math.ceil(Math.random() * 4000)
let resultCount = 0
let lastTime, presentTime

setTimeout( changetoGreen, getRandTime())

function changetoGreen(){
    container.style.backgroundColor = 'rgb(11, 149, 6)'
    container.classList.add('active')
    lastTime = Date.now()
    if(container.classList.contains('active')){
        container.addEventListener( 'click', handleClick)
    }
}

function handleClick(){
    if (container.classList.contains('active')){
        resultCount++
        presentTime = Date.now()
        const newResult = presentTime - lastTime
        const newResultEl = document.createElement('div')
        newResultEl.innerHTML = `<div class="results result${resultCount}">${newResult}ms</div>`
        result.appendChild(newResultEl)
        
        container.style.backgroundColor = 'black'
        container.classList.remove('active')

        if(result.children.length > 10){
            result.firstChild.remove()
        }
        setTimeout(changetoGreen, getRandTime())
    }
}