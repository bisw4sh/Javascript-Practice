function quickSort(arr) {
if (arr.length <= 1) {
    return arr; // Base case: array with 0 or 1 element is already sorted
}

const pivot = arr[Math.floor(arr.length / 2)]; // Choose a pivot element
const less = arr.filter((item) => item < pivot); // Elements less than pivot
const equal = arr.filter((item) => item === pivot); // Elements equal to pivot
const greater = arr.filter((item) => item > pivot); // Elements greater than pivot

return [...quickSort(less), ...equal, ...quickSort(greater)]; // Recursively sort the sub-arrays
}

// Example usage:
const unsortedArray = [3, 1, 4, 1, 5, 9, 2, 6, 5, 3, 5];
const sortedArray = quickSort(unsortedArray);
console.log(sortedArray); // Output: [1, 1, 2, 3, 3, 4, 5, 5, 5, 6, 9]  