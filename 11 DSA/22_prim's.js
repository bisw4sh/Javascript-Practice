class Graph {
  constructor() {
    this.vertices = {};
  }

  addVertex(vertex) {
    this.vertices[vertex] = [];
  }

  addEdge(vertex1, vertex2, weight) {
    this.vertices[vertex1].push({ vertex: vertex2, weight });
    this.vertices[vertex2].push({ vertex: vertex1, weight });
  }

  findMinimumEdge(vertices, visited) {
    let minEdge = { vertex: null, weight: Infinity };
    for (const vertex of vertices) {
      for (const edge of this.vertices[vertex]) {
        if (edge.weight < minEdge.weight && !visited.includes(edge.vertex)) {
          minEdge = { vertex: edge.vertex, weight: edge.weight };
        }
      }
    }
    return minEdge;
  }

  primMST(startVertex) {
    const visited = [];
    const mst = [];

    visited.push(startVertex);

    while (visited.length < Object.keys(this.vertices).length) {
      const minEdge = this.findMinimumEdge(visited, visited);

      visited.push(minEdge.vertex);
      mst.push(minEdge);
    }

    return mst;
  }
}

// Example usage:
const graph = new Graph();

graph.addVertex("A");
graph.addVertex("B");
graph.addVertex("C");
graph.addVertex("D");

graph.addEdge("A", "B", 1);
graph.addEdge("A", "C", 3);
graph.addEdge("B", "C", 4);
graph.addEdge("B", "D", 2);
graph.addEdge("C", "D", 5);

const minimumSpanningTree = graph.primMST("A");
console.log(minimumSpanningTree);