const graph = {
    A: ['B', 'C', 'D'],
    B: ['E', 'F'],
    C: ['G'],
    D: ['H', 'I', 'J'],
    E: ['K'],
    F: ['L', 'M'],
    G: ['N', 'O'],
    H: ['P'],
    I: [],
    J: ['Q', 'R'],
    K: [],
    L: [],
    M: [],
    N: [],
    O: ['S'],
    P: ['T', 'U'],
    Q: [],
    R: ['V'],
    S: [],
    T: [],
    U: [],
    V: []
  };
  
  function depthFirstSearch(graph, node, visited) {
    if (!visited) {
      visited = new Set();
    }
  
    visited.add(node);
    console.log(`Visited node: ${node}`);
  
    const neighbors = graph[node];
    for (const neighbor of neighbors) {
      if (!visited.has(neighbor)) {
        depthFirstSearch(graph, neighbor, visited);
      }
    }
  }
  
  // Usage
  depthFirstSearch(graph, 'A');  