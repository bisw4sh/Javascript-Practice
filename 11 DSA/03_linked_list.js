class Node {
constructor(value) {
    this.value = value;
    this.next = null;
}
}

class LinkedList {
constructor() {
    this.head = null;
    this.tail = null;
}

append(value) {
    const newNode = new Node(value);

    if (!this.head) {
    this.head = newNode;
    this.tail = newNode;
    } else {
    this.tail.next = newNode;
    this.tail = newNode;
    }
}

prepend(value) {
    const newNode = new Node(value);

    if (!this.head) {
    this.head = newNode;
    this.tail = newNode;
    } else {
    newNode.next = this.head;
    this.head = newNode;
    }
}

delete(value) {
    if (!this.head) {
    return;
    }

    if (this.head.value === value) {
    this.head = this.head.next;
    if (!this.head) {
        this.tail = null;
    }
    return;
    }

    let currentNode = this.head;
    while (currentNode.next) {
    if (currentNode.next.value === value) {
        currentNode.next = currentNode.next.next;
        if (!currentNode.next) {
        this.tail = currentNode;
        }
        return;
    }
    currentNode = currentNode.next;
    }
}

toArray() {
    const elements = [];
    let currentNode = this.head;
    while (currentNode) {
    elements.push(currentNode.value);
    currentNode = currentNode.next;
    }
    return elements;
}
}

const list = new LinkedList();
list.append(1);
list.append(2);
list.append(3);
list.prepend(0);
list.delete(2);

console.log(list.toArray()); // Output: [0, 1, 3]