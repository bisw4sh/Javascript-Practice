class TreeNode {
    constructor(value) {
      this.value = value;
      this.left = null;
      this.right = null;
    }
  }
  
  function preorderSearch(node, target) {
    if (node === null) {
      return null; // Target not found in the tree
    }
  
    // Check if the current node's value matches the target
    if (node.value === target) {
      return node; // Target found
    }
  
    // Recursively search in the left and right subtrees
    const leftResult = preorderSearch(node.left, target);
    if (leftResult !== null) {
      return leftResult; // If found in the left subtree, return the result
    }
  
    const rightResult = preorderSearch(node.right, target);
    if (rightResult !== null) {
      return rightResult; // If found in the right subtree, return the result
    }
  
    return null; // Target not found in the entire tree
  }

// Create a binary tree
const root = new TreeNode(1);
root.left = new TreeNode(2);
root.right = new TreeNode(3);
root.left.left = new TreeNode(4);
root.left.right = new TreeNode(5);
root.right.left = new TreeNode(6);
root.right.right = new TreeNode(7);

const targetNode = preorderSearch(root, 5);
if (targetNode !== null) {
  console.log("Target found! Node value:", targetNode.value);
} else {
  console.log("Target not found in the tree.");
}