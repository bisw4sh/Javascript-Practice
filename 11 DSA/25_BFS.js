// Define a graph using an adjacency list representation
const graph = {
    A: ["B", "C"],
    B: ["A", "D", "E"],
    C: ["A", "F", "G"],
    D: ["B"],
    E: ["B"],
    F: ["C"],
    G: ["C"],
  };
  
  // BFS function
  function bfs(graph, startNode) {
    const queue = []; // Initialize a queue for BFS
    const visited = {}; // Keep track of visited nodes
  
    // Add the start node to the queue and mark it as visited
    queue.push(startNode);
    visited[startNode] = true;
  
    while (queue.length > 0) {
      const currentNode = queue.shift(); // Get the front node from the queue
      console.log(currentNode); // Process the current node (you can modify this part based on your requirements)
  
      // Get the neighbors of the current node from the graph
      const neighbors = graph[currentNode];
  
      // Loop through the neighbors
      for (const neighbor of neighbors) {
        // If the neighbor is not visited yet, mark it as visited and add it to the queue
        if (!visited[neighbor]) {
          queue.push(neighbor);
          visited[neighbor] = true;
        }
      }
    }
  }
  
  // Call BFS with a starting node
  bfs(graph, "A");  