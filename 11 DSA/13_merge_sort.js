function mergeSort(arr) {
if (arr.length <= 1) {
    return arr;
}

const mid = Math.floor(arr.length / 2);
const left = mergeSort(arr.slice(0, mid));
const right = mergeSort(arr.slice(mid));

return merge(left, right);
}

function merge(left, right) {
let mergedArray = [];
let leftIndex = 0;
let rightIndex = 0;

while (leftIndex < left.length && rightIndex < right.length) {
    if (left[leftIndex] < right[rightIndex]) {
    mergedArray.push(left[leftIndex]);
    leftIndex++;
    } else {
    mergedArray.push(right[rightIndex]);
    rightIndex++;
    }
}

// Add the remaining elements from both arrays, if any
while (leftIndex < left.length) {
    mergedArray.push(left[leftIndex]);
    leftIndex++;
}

while (rightIndex < right.length) {
    mergedArray.push(right[rightIndex]);
    rightIndex++;
}

return mergedArray;
}

// Example usage:
const unsortedArray = [38, 27, 43, 3, 9, 82, 10];
const sortedArray = mergeSort(unsortedArray);
console.log(sortedArray); // Output: [3, 9, 10, 27, 38, 43, 82]