class TreeNode {
constructor(val, left = null, right = null) {
    this.val = val;
    this.left = left;
    this.right = right;
}
}

function postOrderTraversal(root) {
if (!root) {
    return [];
}

const result = [];

function traverse(node) {
    if (node.left) {
    traverse(node.left);
    }

    if (node.right) {
    traverse(node.right);
    }

    result.push(node.val);
}

traverse(root);
return result;
}

// Example usage:
const tree = new TreeNode(1);
tree.left = new TreeNode(2);
tree.right = new TreeNode(3);
tree.left.left = new TreeNode(4);
tree.left.right = new TreeNode(5);

const result = postOrderTraversal(tree);
console.log(result); // Output: [4, 5, 2, 3, 1]  