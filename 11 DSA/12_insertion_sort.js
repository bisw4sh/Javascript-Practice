function insertionSort(arr) {
const n = arr.length;

for (let i = 1; i < n; i++) {
    let currentElement = arr[i];
    let j = i - 1;

    // Move elements of arr[0..i-1] that are greater than the currentElement
    // to one position ahead of their current position
    while (j >= 0 && arr[j] > currentElement) {
        arr[j + 1] = arr[j];
        j--;
    }

    // Place the currentElement at its correct position
    arr[j + 1] = currentElement;
}

return arr;
}

// Example usage:
const unsortedArray = [64, 34, 25, 12, 22, 11, 90];
const sortedArray = insertionSort(unsortedArray);
console.log(sortedArray); // Output: [11, 12, 22, 25, 34, 64, 90]