// Function to perform Heap Sort
function heapSort(arr) {
    // Build a max heap from the array
    buildMaxHeap(arr);
  
    // Extract elements from the heap one by one
    for (let i = arr.length - 1; i > 0; i--) {
      // Move the current root (maximum element) to the end
      swap(arr, 0, i);
  
      // Call heapify on the reduced heap
      heapify(arr, i, 0);
    }
  
    return arr;
  }
  
  // Function to build a max heap from an array
  function buildMaxHeap(arr) {
    const n = arr.length;
    // Start from the last non-leaf node and heapify all nodes in reverse order
    for (let i = Math.floor(n / 2) - 1; i >= 0; i--) {
      heapify(arr, n, i);
    }
  }
  
  // Function to heapify a subtree rooted at index i in the array of length n
  function heapify(arr, n, i) {
    let largest = i;
    const left = 2 * i + 1;
    const right = 2 * i + 2;
  
    // Check if left child exists and is greater than the root
    if (left < n && arr[left] > arr[largest]) {
      largest = left;
    }
  
    // Check if right child exists and is greater than the root
    if (right < n && arr[right] > arr[largest]) {
      largest = right;
    }
  
    // If the largest element is not the root, swap and heapify the affected subtree
    if (largest !== i) {
      swap(arr, i, largest);
      heapify(arr, n, largest);
    }
  }
  
  // Function to swap two elements in an array
  function swap(arr, i, j) {
    const temp = arr[i];
    arr[i] = arr[j];
    arr[j] = temp;
  }
  
  // Example usage:
  const unsortedArray = [12, 11, 13, 5, 6, 7];
  const sortedArray = heapSort(unsortedArray);
  console.log(sortedArray); // Output: [5, 6, 7, 11, 12, 13]  