// HashTable class
class HashTable {
constructor() {
    this.table = {};
}

// Hash function to convert keys into valid object property names
hash(key) {
    let hashValue = 0;
    for (let i = 0; i < key.length; i++) {
    hashValue += key.charCodeAt(i);
    }
    return hashValue;
}

// Insert a key-value pair into the hash table
insert(key, value) {
    const hashKey = this.hash(key);
    this.table[hashKey] = value;
}

// Get the value associated with a key
get(key) {
    const hashKey = this.hash(key);
    return this.table[hashKey];
}

// Remove a key-value pair from the hash table
remove(key) {
    const hashKey = this.hash(key);
    delete this.table[hashKey];
}

// Check if a key exists in the hash table
contains(key) {
    const hashKey = this.hash(key);
    return this.table.hasOwnProperty(hashKey);
}
}

// Example usage
const hashTable = new HashTable();
hashTable.insert('name', 'John');
hashTable.insert('age', 30);
hashTable.insert('city', 'New York');

console.log(hashTable.get('name')); // Output: John
console.log(hashTable.get('age')); // Output: 30

hashTable.remove('city');
console.log(hashTable.contains('city')); // Output: false