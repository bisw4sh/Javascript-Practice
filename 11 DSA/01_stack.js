class Stack {
  constructor() {
    this.items = [];
  }

  // Push element to the top of the stack
  push(element) {
    this.items.push(element);
  }

  // Remove and return the top element of the stack
  pop() {
    if (this.isEmpty()) {
      return "Underflow";
    }
    return this.items.pop();
  }

  // Return the top element of the stack without removing it
  peek() {
    if (this.isEmpty()) {
      return "No elements in the stack";
    }
    return this.items[this.items.length - 1];
  }

  // Check if the stack is empty
  isEmpty() {
    return this.items.length === 0;
  }

  // Get the size of the stack
  size() {
    return this.items.length;
  }

  // Clear the stack
  clear() {
    this.items = [];
  }

  // Print the elements of the stack
  print() {
    console.log(this.items);
  }
}

// Example usage
const stack = new Stack();
stack.push(10);
stack.push(20);
stack.push(30);
stack.push(40);

console.log("Stack elements:");
stack.print(); // [10, 20, 30, 40]

console.log("Top element:", stack.peek()); // 40

console.log("Popped element:", stack.pop()); // 40

console.log("Stack size:", stack.size()); // 3

console.log("Is the stack empty?", stack.isEmpty()); // false

stack.clear();
console.log("Stack elements after clearing:");
stack.print(); // []
