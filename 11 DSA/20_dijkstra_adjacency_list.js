class PriorityQueue {
  constructor() {
    this.elements = [];
  }

  enqueue(item, priority) {
    this.elements.push({ item, priority });
    this.elements.sort((a, b) => a.priority - b.priority);
  }

  dequeue() {
    return this.elements.shift().item;
  }

  isEmpty() {
    return this.elements.length === 0;
  }
}

function dijkstra(graph, startNode) {
  const distances = {};
  const visited = {};
  const previous = {};
  const priorityQueue = new PriorityQueue();

  // Initialize distances with infinity and start node with 0
  for (let node in graph) {
    distances[node] = Infinity;
  }
  distances[startNode] = 0;

  // Add the start node to the priority queue
  priorityQueue.enqueue(startNode, 0);

  while (!priorityQueue.isEmpty()) {
    const currentNode = priorityQueue.dequeue();

    if (visited[currentNode]) continue;
    visited[currentNode] = true;

    for (let neighbor in graph[currentNode]) {
      const distance = distances[currentNode] + graph[currentNode][neighbor];

      if (distance < distances[neighbor]) {
        distances[neighbor] = distance;
        previous[neighbor] = currentNode;
        priorityQueue.enqueue(neighbor, distance);
      }
    }
  }

  return { distances, previous };
}

// Example usage:

const graph = {
  A: { B: 1, C: 4 },
  B: { A: 1, C: 2, D: 5 },
  C: { A: 4, B: 2, D: 1 },
  D: { B: 5, C: 1 },
};

const startNode = 'A';
const { distances, previous } = dijkstra(graph, startNode);

console.log("Shortest distances:", distances);
console.log("Previous nodes:", previous);