function bubbleSort(arr) {
const n = arr.length;

// Loop through the array
for (let i = 0; i < n - 1; i++) {
// A flag to check if any swaps occurred in this iteration
let swapped = false;

// Inner loop to compare adjacent elements and swap them if necessary
for (let j = 0; j < n - i - 1; j++) {
    if (arr[j] > arr[j + 1]) {
    // Swap elements at positions j and j + 1
    [arr[j], arr[j + 1]] = [arr[j + 1], arr[j]];
    // Set the swapped flag to true
    swapped = true;
    }
}

// If no swaps occurred in this iteration, the array is already sorted
if (!swapped) break;
}

return arr;
}

// Example usage:
const unsortedArray = [64, 34, 25, 12, 22, 11, 90];
const sortedArray = bubbleSort(unsortedArray);
console.log(sortedArray); // Output: [11, 12, 22, 25, 34, 64, 90]