class Graph {
constructor() {
  this.vertices = new Map(); // Use a map to store vertices and their neighbors
}

// Add a new vertex to the graph
addVertex(vertex) {
  if (!this.vertices.has(vertex)) {
    this.vertices.set(vertex, []);
  }
}

// Add an edge between two vertices
addEdge(vertex1, vertex2) {
  if (this.vertices.has(vertex1) && this.vertices.has(vertex2)) {
    const neighbors1 = this.vertices.get(vertex1);
    const neighbors2 = this.vertices.get(vertex2);
    if (!neighbors1.includes(vertex2)) {
      neighbors1.push(vertex2);
    }
    if (!neighbors2.includes(vertex1)) {
      neighbors2.push(vertex1);
    }
  }
}

// Get all the neighbors of a given vertex
getNeighbors(vertex) {
  if (this.vertices.has(vertex)) {
    return this.vertices.get(vertex);
  }
  return [];
}

// Get all the vertices in the graph
getVertices() {
  return Array.from(this.vertices.keys());
}

// Print the graph for visualization
printGraph() {
  for (const [vertex, neighbors] of this.vertices.entries()) {
    console.log(`${vertex} -> ${neighbors.join(', ')}`);
  }
}
}

// Example usage:
const graph = new Graph();
graph.addVertex('A');
graph.addVertex('B');
graph.addVertex('C');
graph.addVertex('D');

graph.addEdge('A', 'B');
graph.addEdge('A', 'C');
graph.addEdge('B', 'D');
graph.addEdge('C', 'D');

graph.printGraph();