class MinHeap {
constructor() {
    this.heap = [];
}

// Get parent index of a node
getParentIndex(index) {
    return Math.floor((index - 1) / 2);
}

// Get left child index of a node
getLeftChildIndex(index) {
    return 2 * index + 1;
}

// Get right child index of a node
getRightChildIndex(index) {
    return 2 * index + 2;
}

// Swap two elements in the heap
swap(index1, index2) {
    const temp = this.heap[index1];
    this.heap[index1] = this.heap[index2];
    this.heap[index2] = temp;
}

// Get the minimum element (root) of the heap
getMin() {
    if (this.heap.length === 0) {
    return null;
    }
    return this.heap[0];
}

// Insert an element into the heap
insert(value) {
    this.heap.push(value);
    this.heapifyUp(this.heap.length - 1);
}

// Restore the heap property by moving an element up the heap
heapifyUp(index) {
    while (index > 0 && this.heap[index] < this.heap[this.getParentIndex(index)]) {
    const parentIndex = this.getParentIndex(index);
    this.swap(index, parentIndex);
    index = parentIndex;
    }
}

// Remove and return the minimum element from the heap
extractMin() {
    if (this.heap.length === 0) {
    return null;
    }

    const minValue = this.heap[0];
    this.heap[0] = this.heap.pop();
    this.heapifyDown(0);
    return minValue;
}

// Restore the heap property by moving an element down the heap
heapifyDown(index) {
    let smallestChildIndex = this.getLeftChildIndex(index);

    if (
    this.getRightChildIndex(index) < this.heap.length &&
    this.heap[this.getRightChildIndex(index)] < this.heap[smallestChildIndex]
    ) {
    smallestChildIndex = this.getRightChildIndex(index);
    }

    if (smallestChildIndex < this.heap.length && this.heap[index] > this.heap[smallestChildIndex]) {
    this.swap(index, smallestChildIndex);
    this.heapifyDown(smallestChildIndex);
    }
}

// Check if the heap is empty
isEmpty() {
    return this.heap.length === 0;
}

// Get the size of the heap
size() {
    return this.heap.length;
}
}

// Example usage:
const heap = new MinHeap();
heap.insert(5);
heap.insert(2);
heap.insert(10);
heap.insert(3);
console.log(heap.extractMin()); // Output: 2
console.log(heap.getMin()); // Output: 3
console.log(heap.size()); // Output: 3  