function selectionSort(arr) {
const n = arr.length;

for (let i = 0; i < n - 1; i++) {
    // Assume the minimum value is at the current index (i).
    let minIndex = i;

    // Find the minimum value in the remaining unsorted part of the array.
    for (let j = i + 1; j < n; j++) {
    if (arr[j] < arr[minIndex]) {
        minIndex = j;
    }
    }

    // Swap the found minimum element with the element at index i.
    if (minIndex !== i) {
    const temp = arr[i];
    arr[i] = arr[minIndex];
    arr[minIndex] = temp;
    }
}

return arr;
}

// Example usage:
const unsortedArray = [64, 25, 12, 22, 11];
const sortedArray = selectionSort(unsortedArray);
console.log(sortedArray); // Output: [11, 12, 22, 25, 64]