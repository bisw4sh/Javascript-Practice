class Graph {
    constructor() {
      this.nodes = new Set();
      this.edges = new Map();
    }
  
    addNode(node) {
      this.nodes.add(node);
      this.edges.set(node, []);
    }
  
    addEdge(node1, node2, weight) {
      this.edges.get(node1).push({ node: node2, weight });
      this.edges.get(node2).push({ node: node1, weight });
    }
  
    primMST(startNode) {
      const mst = new Set();
      const edges = new Map();
      const nodes = new Set(this.nodes);
  
      nodes.delete(startNode);
  
      this.edges.get(startNode).forEach((edge) => {
        edges.set(edge.node, edge.weight);
      });
  
      while (nodes.size > 0) {
        let minEdgeNode;
        let minEdgeWeight = Infinity;
  
        for (const node of nodes) {
          if (edges.has(node) && edges.get(node) < minEdgeWeight) {
            minEdgeNode = node;
            minEdgeWeight = edges.get(node);
          }
        }
  
        if (!minEdgeNode) {
          break;
        }
  
        mst.add(minEdgeNode);
        nodes.delete(minEdgeNode);
  
        this.edges.get(minEdgeNode).forEach((edge) => {
          if (nodes.has(edge.node) && edge.weight < edges.get(edge.node)) {
            edges.set(edge.node, edge.weight);
          }
        });
      }
  
      return mst;
    }
  }
  
  // Example usage:
  const graph = new Graph();
  
  graph.addNode("A");
  graph.addNode("B");
  graph.addNode("C");
  graph.addNode("D");
  graph.addNode("E");
  
  graph.addEdge("A", "B", 2);
  graph.addEdge("A", "C", 3);
  graph.addEdge("B", "C", 1);
  graph.addEdge("B", "D", 1);
  graph.addEdge("C", "D", 2);
  graph.addEdge("B", "E", 4);
  graph.addEdge("D", "E", 5);
  
  const minimumSpanningTree = graph.primMST("A");
  console.log("Minimum Spanning Tree:", minimumSpanningTree); // Output: Set { 'A', 'B', 'C', 'D' }  