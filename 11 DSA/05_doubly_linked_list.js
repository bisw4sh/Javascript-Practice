// Define a Node class for individual nodes in the doubly linked list
class Node {
    constructor(data) {
      this.data = data;
      this.previous = null;
      this.next = null;
    }
  }
  
  // Define the DoublyLinkedList class
  class DoublyLinkedList {
    constructor() {
      this.head = null;
      this.tail = null;
    }
  
    // Method to add a new node to the end of the list
    append(data) {
      const newNode = new Node(data);
  
      if (!this.head) {
        // If the list is empty, set the new node as both the head and the tail
        this.head = newNode;
        this.tail = newNode;
      } else {
        // If the list is not empty, update the pointers of the current tail and the new node
        newNode.previous = this.tail;
        this.tail.next = newNode;
        this.tail = newNode;
      }
    }
  
    // Method to add a new node to the beginning of the list
    prepend(data) {
      const newNode = new Node(data);
  
      if (!this.head) {
        // If the list is empty, set the new node as both the head and the tail
        this.head = newNode;
        this.tail = newNode;
      } else {
        // If the list is not empty, update the pointers of the current head and the new node
        newNode.next = this.head;
        this.head.previous = newNode;
        this.head = newNode;
      }
    }
  
    // Method to insert a new node at a specific position in the list
    insertAt(data, position) {
      if (position < 0 || position > this.size()) {
        console.log("Invalid position");
        return;
      }
  
      if (position === 0) {
        this.prepend(data);
        return;
      }
  
      if (position === this.size()) {
        this.append(data);
        return;
      }
  
      const newNode = new Node(data);
      let currentNode = this.head;
      let count = 0;
  
      while (count < position - 1) {
        currentNode = currentNode.next;
        count++;
      }
  
      newNode.previous = currentNode;
      newNode.next = currentNode.next;
      currentNode.next.previous = newNode;
      currentNode.next = newNode;
    }
  
    // Method to remove a node from the list
    remove(data) {
      let currentNode = this.head;
  
      while (currentNode) {
        if (currentNode.data === data) {
          if (currentNode === this.head && currentNode === this.tail) {
            // If there's only one node in the list
            this.head = null;
            this.tail = null;
          } else if (currentNode === this.head) {
            // If the node to be removed is the head
            this.head = currentNode.next;
            this.head.previous = null;
          } else if (currentNode === this.tail) {
            // If the node to be removed is the tail
            this.tail = currentNode.previous;
            this.tail.next = null;
          } else {
            // If the node to be removed is in the middle of the list
            currentNode.previous.next = currentNode.next;
            currentNode.next.previous = currentNode.previous;
          }
          break;
        }
        currentNode = currentNode.next;
      }
    }
  
    // Method to get the size of the list
    size() {
      let count = 0;
      let currentNode = this.head;
  
      while (currentNode) {
        count++;
        currentNode = currentNode.next;
      }
  
      return count;
    }
  
    // Method to print the list in forward direction
    printForward() {
      let currentNode = this.head;
      const result = [];
  
      while (currentNode) {
        result.push(currentNode.data);
        currentNode = currentNode.next;
      }
  
      console.log("Forward:", result.join(" -> "));
    }
  
    // Method to print the list in reverse direction
    printBackward() {
      let currentNode = this.tail;
      const result = [];
  
      while (currentNode) {
        result.push(currentNode.data);
        currentNode = currentNode.previous;
      }
  
      console.log("Backward:", result.join(" -> "));
    }
  }
  
  // Example usage
  const list = new DoublyLinkedList();
  
  list.append(1);
  list.append(2);
  list.append(3);
  
  list.prepend(0);
  
  list.insertAt(4, 4);
  list.insertAt(5, 2);
  
  list.printForward();   // Output: Forward: 0 -> 1 -> 5 -> 2 -> 3 -> 4
  list.printBackward();  // Output: Backward: 4 -> 3 -> 2 -> 5 -> 1 -> 0
  
  list.remove(2);
  
  list.printForward();   // Output: Forward: 0 -> 1 -> 5 -> 3 -> 4
  list.printBackward();  // Output: Backward: 4 -> 3 -> 5 -> 1 -> 0  