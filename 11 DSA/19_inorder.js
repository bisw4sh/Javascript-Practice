class TreeNode {
    constructor(val) {
      this.val = val;
      this.left = null;
      this.right = null;
    }
  }

  function inOrderTraversalRecursive(root, result = []) {
    if (!root) return result;
  
    inOrderTraversalRecursive(root.left, result);
    result.push(root.val);
    inOrderTraversalRecursive(root.right, result);
  
    return result;
  }
// Creating the binary tree
const root = new TreeNode(1);
root.left = new TreeNode(2);
root.right = new TreeNode(3);
root.left.left = new TreeNode(4);
root.left.right = new TreeNode(5);

// Recursive In-order traversal
const recursiveResult = inOrderTraversalRecursive(root);
console.log("Recursive In-order Traversal:", recursiveResult); // Output: [4, 2, 5, 1, 3]  