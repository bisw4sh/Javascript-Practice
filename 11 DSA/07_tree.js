// Define a Node class for tree nodes
class Node {
    constructor(value) {
      this.value = value;
      this.children = [];
    }
    
    addChild(node) {
      this.children.push(node);
    }
  }
  
  // Create a tree
  const tree = new Node(1);
  
  // Add children to the root node
  const node2 = new Node(2);
  const node3 = new Node(3);
  tree.addChild(node2);
  tree.addChild(node3);
  
  // Add children to node 2
  const node4 = new Node(4);
  const node5 = new Node(5);
  node2.addChild(node4);
  node2.addChild(node5);
  
  // Add children to node 3
  const node6 = new Node(6);
  const node7 = new Node(7);
  node3.addChild(node6);
  node3.addChild(node7);
  