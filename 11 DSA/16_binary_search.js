function binarySearch(arr, target) {
let left = 0;
let right = arr.length - 1;

while (left <= right) {
    const mid = Math.floor((left + right) / 2);

    if (arr[mid] === target) {
    return mid; // Found the target value at index mid
    } else if (arr[mid] < target) {
    left = mid + 1; // Target is in the right half of the array
    } else {
    right = mid - 1; // Target is in the left half of the array
    }
}

return -1; // Target value not found in the array
}

// Example usage:
const sortedArray = [2, 5, 8, 12, 16, 23, 38, 45, 56, 72, 91];
const targetValue = 23;
const index = binarySearch(sortedArray, targetValue);
console.log(`Target value ${targetValue} found at index ${index}`);  