class Node {
constructor(data) {
    this.data = data;
    this.previous = null;
    this.next = null;
}
}

class DoublyLinkedList {
constructor() {
    this.head = null;
    this.tail = null;
    this.size = 0;
}

// Add a node to the end of the list
append(data) {
    const newNode = new Node(data);

    if (!this.head) {
    this.head = newNode;
    this.tail = newNode;
    } else {
    newNode.previous = this.tail;
    this.tail.next = newNode;
    this.tail = newNode;
    }

    this.size++;
}

// Insert a node at a specific position in the list
insertAt(data, position) {
    if (position < 0 || position > this.size) {
    throw new Error('Invalid position');
    }

    const newNode = new Node(data);

    if (position === 0) {
    if (!this.head) {
        this.head = newNode;
        this.tail = newNode;
    } else {
        newNode.next = this.head;
        this.head.previous = newNode;
        this.head = newNode;
    }
    } else if (position === this.size) {
    newNode.previous = this.tail;
    this.tail.next = newNode;
    this.tail = newNode;
    } else {
    let current = this.head;
    let index = 0;

    while (index < position - 1) {
        current = current.next;
        index++;
    }

    newNode.previous = current;
    newNode.next = current.next;
    current.next.previous = newNode;
    current.next = newNode;
    }

    this.size++;
}

// Remove a node from the list
remove(data) {
    let current = this.head;

    while (current) {
    if (current.data === data) {
        if (current === this.head && current === this.tail) {
        this.head = null;
        this.tail = null;
        } else if (current === this.head) {
        this.head = current.next;
        this.head.previous = null;
        } else if (current === this.tail) {
        this.tail = current.previous;
        this.tail.next = null;
        } else {
        current.previous.next = current.next;
        current.next.previous = current.previous;
        }

        this.size--;
        return;
    }

    current = current.next;
    }
}

// Get the size of the list
getSize() {
    return this.size;
}

// Print the list
print() {
    let current = this.head;
    let result = '';

    while (current) {
    result += current.data + ' ';
    current = current.next;
    }

    console.log(result);
}
}

// Usage example:
const list = new DoublyLinkedList();

list.append(1);
list.append(2);
list.append(3);

list.print(); // Output: 1 2 3

list.insertAt(4, 1);
list.insertAt(5, 3);

list.print(); // Output: 1 4 2 5 3

list.remove(2);

list.print(); // Output: 1 4 5 3  