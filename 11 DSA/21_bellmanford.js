// Represents an edge between two vertices with a weight
class Edge {
    constructor(source, destination, weight) {
      this.source = source;
      this.destination = destination;
      this.weight = weight;
    }
  }
  
  // Represents a graph with a list of edges
  class Graph {
    constructor(vertices) {
      this.vertices = vertices;
      this.edges = [];
    }
  
    addEdge(source, destination, weight) {
      this.edges.push(new Edge(source, destination, weight));
    }
  
    // Bellman-Ford algorithm to find the shortest paths
    bellmanFord(startVertex) {
      // Initialize distances from the start vertex to all other vertices as Infinity
      const distances = {};
      for (const vertex of this.vertices) {
        distances[vertex] = Infinity;
      }
      distances[startVertex] = 0;
  
      // Relax edges repeatedly |V| - 1 times
      for (let i = 0; i < this.vertices.length - 1; i++) {
        for (const edge of this.edges) {
          const { source, destination, weight } = edge;
          if (distances[source] !== Infinity && distances[source] + weight < distances[destination]) {
            distances[destination] = distances[source] + weight;
          }
        }
      }
  
      // Check for negative cycles
      for (const edge of this.edges) {
        const { source, destination, weight } = edge;
        if (distances[source] !== Infinity && distances[source] + weight < distances[destination]) {
          throw new Error("Graph contains a negative-weight cycle");
        }
      }
  
      return distances;
    }
  }
  
  // Example usage:
  const graph = new Graph(["A", "B", "C", "D", "E"]);
  
  graph.addEdge("A", "B", 5);
  graph.addEdge("A", "C", 2);
  graph.addEdge("B", "D", 3);
  graph.addEdge("C", "D", 1);
  graph.addEdge("D", "E", -4);
  
  const startVertex = "A";
  const distances = graph.bellmanFord(startVertex);
  console.log(distances);  