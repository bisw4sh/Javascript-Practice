const images = ['c.png', 'dart.png', 'java.png', 'js.png', 'php.png', 'python.png','rust.png', 'solidity.png', 'c.png', 'dart.png', 'java.png', 'js.png', 'php.png', 'python.png','rust.png', 'solidity.png']
const imgSrc = document.querySelectorAll('.square img')
const finalScreenArr = Array.from(imgSrc)
let lastClicked = null

function shuffle(array) {
    //fisher-yates shuffle
    let currentIndex = array.length,  randomIndex
  
    // While there remain elements to shuffle.
    while (currentIndex != 0) {
  
      // Pick a remaining element.
      randomIndex = Math.floor(Math.random() * currentIndex)
      currentIndex--
  
      // And swap it with the current element.
      [array[currentIndex], array[randomIndex]] = [
        array[randomIndex], array[currentIndex]]
    }
  
    return array
}

shuffle(images)

imgSrc.forEach( (imageSquare, index) => {
    imageSquare.src = `./imgs/${images[index]}`
    imageSquare.setAttribute('data-value', `${images[index]}`)
    imageSquare.addEventListener('click',(e) => revealed(e))
})

function revealed(eventEl){
    if(lastClicked === eventEl.target) return
    eventEl.target.setAttribute('data-selected', 'true')
    eventEl.target.parentNode.childNodes[1].style.backgroundColor = 'unset'

    if(lastClicked && lastClicked.getAttribute('data-value') === eventEl.target.getAttribute('data-value')){
        lastClicked.classList.add('hide')
        eventEl.target.classList.add('hide')
        eventEl.target.parentNode.style.backgroundColor = 'unset'
        lastClicked = null

        //If all square are disappeared
        const finalScreen = finalScreenArr.every( image => image.getAttribute('data-selected') === 'true')
        if(finalScreen) document.body.innerHTML = `You completed the game`
    }
    else if(lastClicked && lastClicked.getAttribute('data-value') != eventEl.target.getAttribute('data-value')){
        lastClicked.setAttribute('data-selected', '')
        eventEl.target.setAttribute('data-selected', '')
        lastClicked.style.backgroundColor = 'unset'
        eventEl.target.style.backgroundColor = 'unset'
        lastClicked = null
    }
    else{
        lastClicked = eventEl.target
        eventEl.target.style.backgroundColor = 'unset'
    }

}